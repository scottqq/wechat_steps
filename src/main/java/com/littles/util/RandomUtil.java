package com.littles.util;

import java.util.Random;

public class RandomUtil {
	
	private final static Random rnd = new Random();
	
	/**
	 * 生成[min,max]之间的随机数
	 */
	public static int getRandomStep(int min, int max) {
		int delta = max - min + 1;
		return (int)(rnd.nextDouble()*delta + min);
	}
	
}
