package com.littles.wechat.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import com.littles.wechat.steps.App;

public class StepUI extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	private JLabel appIdLabel = new JLabel("乐动ID");
	private JLabel stepLabel = new JLabel("步数");
	
	private JTextField appIDText = new JTextField(20);
	private JTextField stepText = new JTextField(20);
	
	private JButton btn = new JButton("刷新步数");
	
	public StepUI() 
	{
		initComponents();
		initEvents();
	}
	
	private void initComponents()
	{
		this.setTitle("微信刷步数");
		this.setSize(200, 200);
		this.setLayout(new BorderLayout());
		
		JPanel wp = new JPanel();
		wp.setLayout(new GridLayout(2, 1, 5, 5));
		wp.add(appIdLabel);
		wp.add(stepLabel);
		this.add(wp,BorderLayout.WEST);
		
		JPanel cp = new JPanel();
		cp.setLayout(new GridLayout(2, 1, 5, 5));
		
		cp.add(appIDText);
		
		cp.add(stepText);
		this.add(cp, BorderLayout.CENTER);
		
		JPanel sp = new JPanel();
		sp.add(btn);
		this.add(sp,BorderLayout.SOUTH);
		this.setSize(600, 400);
		
		this.setLocationRelativeTo(null);
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	private void initEvents()
	{
		btn.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		int steps = 0;
		String uid = "";
		
		if(e.getSource() == btn)
		{
			uid = appIDText.getText();
			
			if(!StringUtils.isNumeric(uid))
			{
				JOptionPane.showMessageDialog(this, "请输入正确的乐动ID");
				return;
			}
			
			String strStep = stepText.getText();
			if(!StringUtils.isNumeric(strStep))
			{
				JOptionPane.showMessageDialog(this, "请输入正确的步数");
				return;
			}
			steps = Integer.parseInt(strStep);
			
			if(App.resetSteps(uid, steps))
			{
				JOptionPane.showMessageDialog(this, "恭喜您,装逼成功!");
			}
			
		}
	}
	
}
