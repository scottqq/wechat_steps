package com.littles.wechat.steps;

import java.util.HashMap;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.littles.wechat.ui.StepUI;

/**
 * @author scottdu
 */
public class App 
{
	
	public static boolean resetSteps(String uid, int steps)
	{
		try
		{
			int date = (int) (System.currentTimeMillis()/1000);
			Connection conn = Jsoup.connect("http://pl.api.ledongli.cn/xq/io.ashx").header("Content-Type", "application/x-www-form-urlencode;charset=UTF-8");
			conn = conn.header("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.4; MI 4LTE MIUI/5.8.27)");
			conn = conn.header("Host", "pl.api.ledongli.cn");
			conn = conn.header("Connection", "Keep-Alive");
			conn = conn.header("Accept-Encoding", "gzip");
			Map<String,String> params = new HashMap<String,String>();
			String key = "[{\"distance\":0,\"duration\":0,\"report\":\"[]\",\"pm2d5\":0,\"date\":"+date+",\"activeValue\":108,\"steps\":"+steps+",\"calories\":0}]";
			params.put("action", "profile");
			params.put("pc", "29e39ed274ea8e7a50f8a83abf1239faca843022");
			params.put("cmd", "updatedaily");
			params.put("uid", uid);
			params.put("list", key);
			conn = conn.data(params);
			conn = conn.method(Method.POST);
			Response resp = conn.execute();
			String respBody = resp.body();
			//{"status": "OK", "uid": null, "ret": null, "errorCode": 0, "error": null, "path": null}
			if(respBody != null && (respBody.contains("OK") || respBody.contains("ok")))
			{
				return true;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
    public static void main( String[] args ) throws Exception
    {
//    	boolean flag = resetSteps("44742689",93647);
//    	System.out.println(flag);
    	new StepUI().setVisible(true);
    }
}
