package com.littles.wechat.steps;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.littles.util.RandomUtil;

public class TestRandomUtil {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		int min = 20000;
		int max = 99800;
		for(int i = 0; i < 1000000; i++)
		{
			int ret = RandomUtil.getRandomStep(min, max);
			Assert.assertTrue(ret <= max && ret >= min);
		}
	}

}
