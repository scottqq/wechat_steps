package com.littles.wechat.steps;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.littles.util.RandomUtil;

public class TestApp {
	
	private static final String[] UID_ARRAY = new String[]{"44742689","44960338","44815567","47350669"};
	
	private int steps = 25000;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		steps = RandomUtil.getRandomStep(20000, 99800);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_000_dzx() {
		String uid = UID_ARRAY[0];
		
		boolean actual = App.resetSteps(uid, steps);
		
		if(actual) {
			System.out.println("dzx updated " + steps + " successfully!!!");
		}
		else {
			System.out.println("dzx update " + steps + " fail");
		}
		Assert.assertTrue(actual);
	}
	
	@Test
	public void test_001_jyq() {
		String uid = UID_ARRAY[1];
		
		boolean actual = App.resetSteps(uid, steps);
		
		if(actual) {
			System.out.println("jyq updated " + steps + " successfully!!!");
		}
		else {
			System.out.println("jyq update " + steps + " fail");
		}
		Assert.assertTrue(actual);
	}
	
//	@Test
//	public void test_002_tq() {
//		String uid = UID_ARRAY[2];
//		
//		boolean actual = App.resetSteps(uid, steps);
//		
//		if(actual) {
//			System.out.println("tq updated " + steps + " successfully!!!");
//		}
//		else {
//			System.out.println("tq update " + steps + " fail");
//		}
//		Assert.assertTrue(actual);
//	}
	
//	@Test
//	public void test_003_sej() {
//		String uid = UID_ARRAY[3];
//		
//		boolean actual = App.resetSteps(uid, steps);
//		
//		if(actual) {
//			System.out.println("sej updated " + steps + " successfully!!!");
//		}
//		else {
//			System.out.println("sej update " + steps + " fail");
//		}
//		Assert.assertTrue(actual);
//	}
}
